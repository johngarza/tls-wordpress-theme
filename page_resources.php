<?php

/* 
 * Template Name: Resources
 */

if(get_field('header_image')) {
    $hero_background = get_field('header_image');
} else {
    $hero_background = get_field('banner_image', 'option');
}

if(get_field('fancy_title')) {
    $title = get_field('fancy_title');
} else {
    $title = get_the_title();
}

get_header(); ?>

    <section class="inner-hero-container" style="background-image: url(<?php echo $hero_background; ?>);">
        <div class="gradient">
            <div class="hero-header row">
                <div class="small-12 columns text-center">
                    <h1><?php echo $title; ?></h1>
                    <p><?php the_field('res_hero_content'); ?></p>
                </div>
            </div>
        </div>
    </section>
    <div class="res-content entry-content">
        <section class="resource-block-container">
            <?php if(have_rows('res_resource_blocks_repeater')) :?>

                <?php $i = 0; ?>
                <?php while(have_rows('res_resource_blocks_repeater')) : the_row(); ?>
                    <div class="expanded row collapse resource-row" data-id=<?php echo $i;?>>
                        <div class="small-12 large-6 columns resource-image" style="background-image: url(<?php the_sub_field('res_resource_block_image'); ?>);"></div>
                        <div class="small-12 large-6 columns resource-content">
                            <div class="row">
                                <div class="small-12 columns">
                                    <h4><?php the_sub_field('res_resource_block_header'); ?></h4>
                                    <p><?php the_sub_field('res_resource_block_text'); ?></p>
                                    <a href="<?php the_sub_field('res_resource_block_url'); ?>"><i class="fa fa-arrow-right" aria-hidden="true"></i> <?php the_sub_field('res_resource_block_link_text'); ?></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php $i++; ?>
                <?php endwhile; ?>

            <?php endif; ?>
        </section>

        <section class="faq-container">
            <div class="row align-middle">
                <div class="small-12 large-3 columns text-center">
                    <a href="<?php the_field('res_faq_link_url'); ?>"><h3><?php the_field('res_faq_header'); ?></h3></a>
                </div>
                <div class="small-12 large-9 columns">
                    <?php the_field('res_faq_text'); ?>
                    <a href="<?php the_field('res_faq_link_url'); ?>" class="link"><i class="fa fa-arrow-right" aria-hidden="true"></i> <?php the_field('res_faq_link_text'); ?></a>
                </div>
            </div>
        </section>
        <section class="more-resources">
            <div class="row">
                <div class="column">
                    <h3><?php the_field('res_more_header'); ?></h3>
                    <p><?php the_field('res_more_text'); ?></p>
                </div>
            </div>
            <?php if(have_rows('res_more_resources_repeater')) : ?>
                <div class="row text-center more-resources-row">
                    <?php while(have_rows('res_more_resources_repeater')) : the_row(); ?>
                        <div class="small-12 medium-6 large-4 columns">
                            <a href="<?php the_sub_field('res_more_res_url'); ?>">
                                <div class="more-boxes">
                                    <h4><?php the_sub_field('res_more_res_title'); ?></h4>
                                    <?php the_sub_field('res_more_res_content'); ?>
                                    <p class="link"><i class="fa fa-arrow-right" aria-hidden="true"></i> Learn more</p>
                                </div>
                            </a>
                        </div>
                    <?php endwhile; ?>
                </div>
            <?php endif; ?>
            <div class="row text-center suggested-resources">
                <div class="column">
                    <a href="<?php the_field('res_button_url'); ?>" class="btn btnCTA"><?php the_field('res_button_text'); ?></a>
                    <a href="<?php the_field('res_link_url'); ?>" class="link"><i class="fa fa-arrow-right" aria-hidden="true"></i> <?php the_field('res_link_text'); ?></a>
                </div>
            </div>
        </section>
    </div>
<?php get_footer(); ?>

<script>
    (function($){
        $('.resource-row').each(function(){
            if($(this).data('id') % 2 === 0) {
                $(this).find('.resource-image').addClass('large-order-1');
                $(this).find('.resource-content').addClass('large-order-2');
            } else {
                $(this).find('.resource-image').addClass('large-order-2');
                $(this).find('.resource-content').addClass('large-order-1');
            }
        
        });
        
    })(jQuery);
</script>