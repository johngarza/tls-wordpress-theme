<?php get_header(); ?>
	<section class="inner-hero-container" style="background-image: url(<?php the_field('banner_image', 'option') ?>);">
            <div class="gradient">
                <div class="hero-header row">
                    <div class="small-12 columns text-center">
                        <h1 class="archive-title"><?php _e( 'Search Results for:', 'jointswp' ); ?> <?php echo esc_attr(get_search_query()); ?></h1>	
                    </div>
                </div>
            </div>
        </section>		
	<div id="content">

		<div id="inner-content" class="search-page">

				<?php if (have_posts()) : ?>
                                    <div class="row small-up-1 large-up-3" id="ms-container">
                                        <?php while (have_posts()) : the_post(); ?>

                                                <!-- To see additional archive styles, visit the /parts directory -->
                                                <?php get_template_part( 'parts/loop', 'archive' ); ?>

                                        <?php endwhile; ?>	
                                    </div>
				
                                    <?php joints_page_navi(); ?>
					
				<?php else : ?>
                                    <div class="row">
                                        <div class="small-12 large-8 columns">
                                            <?php get_template_part( 'parts/content', 'missing' ); ?>
                                        </div>
                                        <div class="small-12 large-4 columns default-sidebar">
                                            <?php get_sidebar(); ?>
                                            <?php if(have_rows('default_sidebar_buttons', 'option')) : ?>
                                                <?php while(have_rows('default_sidebar_buttons', 'option')) : the_row(); ?>
                                                    <a href="<?php the_sub_field('default_sidebar_button_url');  ?>" class="btn btnCTA text-center"><?php the_sub_field('default_sidebar_button_text'); ?></a>
                                                <?php endwhile; ?>
                                            <?php endif; ?>
                                        </div>
                                    </div>	
			    <?php endif; ?>
		
		</div> <!-- end #inner-content -->

	</div> <!-- end #content -->

<?php get_footer(); ?>
