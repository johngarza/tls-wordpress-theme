<?php 

if(get_field('header_image')) {
    $hero_background = get_field('header_image');
} else {
    $hero_background = get_field('banner_image', 'option');
}

if(get_field('fancy_title')) {
    $title = get_field('fancy_title');
} else {
    $title = get_the_title();
}

get_header(); ?>

    <section class="inner-hero-container" style="background-image: url(<?php echo $hero_background; ?>);">
        <div class="gradient">
            <div class="hero-header row">
                <div class="small-12 columns text-center">
                    <h1><?php echo $title; ?></h1>
                </div>
            </div>
        </div>
    </section>

    <div id="content">

	<div id="inner-content" class="row">

		<main id="main" class="large-8 medium-8 columns" role="main">
		
		    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		
		    	<?php get_template_part( 'parts/loop', 'single' ); ?>
		    	
		    <?php endwhile; else : ?>
		
		   		<?php get_template_part( 'parts/content', 'missing' ); ?>

		    <?php endif; ?>

		</main> <!-- end #main -->
                <div class="small-12 large-4 columns default-sidebar">
                    <?php get_sidebar(); ?>
                    <?php if(have_rows('default_sidebar_buttons', 'option')) : ?>
                        <?php while(have_rows('default_sidebar_buttons', 'option')) : the_row(); ?>
                            <a href="<?php the_sub_field('default_sidebar_button_url');  ?>" class="btn btnCTA text-center"><?php the_sub_field('default_sidebar_button_text'); ?></a>
                        <?php endwhile; ?>
                    <?php endif; ?>
                    
                </div>

	</div> <!-- end #inner-content -->

</div> <!-- end #content -->

<?php get_footer(); ?>