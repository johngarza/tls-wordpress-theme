<?php get_header(); ?>
	<section class="inner-hero-container" style="background-image: url(<?php the_field('banner_image', 'option') ?>);">
            <div class="gradient">
                <div class="hero-header row">
                    <div class="small-12 columns text-center">
                        <h1><?php _e( 'Epic 404 - Article Not Found', 'jointswp' ); ?></h1>
                    </div>
                </div>
            </div>
        </section>		
	<div id="content">

		<div id="inner-content" class="row">
	
			<main id="main" class="large-8 medium-8 columns" role="main">

				<article id="content-not-found">
			
					<section class="entry-content">
						<p><?php _e( 'The article you were looking for was not found, but maybe try looking again!', 'jointswp' ); ?></p>
					</section> <!-- end article section -->

					<section class="search">
					    <p><?php get_search_form(); ?></p>
					</section> <!-- end search section -->
			
				</article> <!-- end article -->
	
			</main> <!-- end #main -->

		</div> <!-- end #inner-content -->

	</div> <!-- end #content -->

<?php get_footer(); ?>