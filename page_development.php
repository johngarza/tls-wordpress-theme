<?php

/* 
 * Template Name: Development on Demand
 */

if(get_field('header_image')) {
    $hero_background = get_field('header_image');
} else {
    $hero_background = get_field('banner_image', 'option');
}

if(get_field('fancy_title')) {
    $title = get_field('fancy_title');
} else {
    $title = get_the_title();
}

get_header(); ?>

    <section class="inner-hero-container" style="background-image: url(<?php echo $hero_background; ?>);">
        <div class="gradient">
            <div class="hero-header row">
                <div class="small-12 columns text-center">
                    <h1><?php echo $title; ?></h1>
                    <p><?php the_field('dd_hero_text'); ?></p>
                </div>
            </div>
        </div>
    </section>


         <section class="utsa-tabs dd-tabs">
            <div class="expanded row tabs collapse sticky" id="example-tabs" data-magellan>        
                <div class="small-12 medium-4 columns tabs-title"><a href="#teaching">Teaching<span><i class="fa fa-arrow-down" aria-hidden="true"></i>Learn More</span></a></div>
                <div class="small-12 medium-4 columns tabs-title"><a href="#learning">Learning<span><i class="fa fa-arrow-down" aria-hidden="true"></i>Learn More</span></a></div>
                <div class="small-12 medium-4 columns tabs-title"><a href="#connecting">Connecting<span><i class="fa fa-arrow-down" aria-hidden="true"></i>Learn More</span></a></div>
            </div>
        </section>

        <div class="dd-content entry-content">
            <section id="teaching" class="teaching-container" data-magellan-target='teaching'>
                <div class="row">
                    <div class="column">
                        <h3>Teaching</h3>
                        <p><?php the_field('dd_teaching_text'); ?></p>
                    </div>
                </div>
                <?php if(have_rows('dd_teaching_sections')) : ?>
                    <?php $row = 1; ?>
                    <?php while(have_rows('dd_teaching_sections')) : the_row(); ?>
                        <?php $extra = ''; ?>
                        <?php if($row > 3) {
                            $extra = 'onclick-1';
                        } ?>
                        <div class='row topic-row <?php echo $extra; ?>'>
                            <div class='small-12 large-4 columns'>
                                <h5><?php the_sub_field('dd_teaching_mod_title'); ?></h5>
                                <p><?php the_sub_field('dd_teaching_mod_text');?></p>
                            </div>
                            <div class='small-12 large-8 columns show-for-large'>
                                <?php if(have_rows('dd_teaching_modules')) : ?>
                                    <div class="orbit" role="region" aria-label="Resources" data-orbit> 
                                        <?php $slides = 0;?>
                                        <div class='resource-group  orbit-slide'>
                                            <?php $slides++; ?>
                                            <div class='row'>
                                                <?php $count = 0; ?>
                                                <?php while(have_rows('dd_teaching_modules')) : ?>
                                                    <?php if ($count > 0 && ($count % 2 == 0)) :?>
                                                        </div>
                                                        </div>

                                                        <div class='resource-group  orbit-slide'>
                                                            <?php $slides++; ?>
                                                            <div class='row'>
                                                    <?php endif; ?>   
                                                    <?php the_row(); ?>

                                                    <div class='small-12 large-6 columns'>
                                                        <div class='mod-box'>
                                                            <div class='mod-image' style='background-image: url(<?php the_sub_field('dd_teaching_mod_box_image'); ?>);'>
                                                                <div class="gradient">
                                                                    <a href='<?php the_sub_field('dd_teaching_mod_box_url'); ?>' class='btn btnWhite'>View Details</a>
                                                                </div>
                                                            </div>
                                                            <div class='mod-info'>
                                                                <h6><?php the_sub_field('dd_teaching_mod_box_title'); ?></h6>
                                                                <p><?php the_sub_field('dd_teaching_mod_box_text'); ?></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php $count++;?>
                                                                
                                                <?php endwhile; ?>
                                            </div>
                                        </div>
                                        <?php if($slides > 1) :?>
                                            <nav class="orbit-bullets">
                                                <?php $slide_count = 0; ?>
                                                <?php while($slides > 0) : ?>
                                                    <button <?php if($slide_count == 0) { echo "class='is-active'"; } ?>  data-slide="<?php echo $slide_count; ?>"><span class="show-for-sr">First slide details.</span><span class="show-for-sr">Current Slide</span></button>
                                                    <?php $slides--; ?>
                                                    <?php $slide_count++; ?>
                                                <?php endwhile; ?>
                                            </nav>
                                        <?php endif; ?>
                                    </div>
                                <?php endif; ?>
                            </div>
                            
                            <div class='hide-for-large small-12 large-8 columns'>
                                <?php if(have_rows('dd_teaching_modules')) : ?>
                                    <div class="orbit" role="region" aria-label="Resources" data-orbit>
                                        <div class='row'>
                                            <?php while(have_rows('dd_teaching_modules')) : the_row(); ?>
                                                <div class='small-12 large-6 columns orbit-slide'>
                                                    <div class='mod-box'>
                                                        <div class='mod-image' style='background-image: url(<?php the_sub_field('dd_teaching_mod_box_image'); ?>);'>
                                                            <div class="gradient">
                                                                <a href='<?php the_sub_field('dd_teaching_mod_box_url'); ?>' class='btn btnWhite'>View Details</a>
                                                            </div>
                                                        </div>
                                                        <div class='mod-info'>
                                                            <h6><?php the_sub_field('dd_teaching_mod_box_title'); ?></h6>
                                                            <p><?php the_sub_field('dd_teaching_mod_box_text'); ?></p>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php endwhile; ?>
                                        </div>
                                        <nav class="orbit-bullets">
                                            <?php $mobile_slides = 0 ?>
                                            <?php while(have_rows('dd_teaching_modules')) : the_row(); ?>
                                                <button <?php if($mobile_slides == 0) { echo "class='is-active'"; } ?>  data-slide="<?php echo $mobile_slides; ?>"><span class="show-for-sr">First slide details.</span><span class="show-for-sr">Current Slide</span></button>
                                                <?php $mobile_slides++; ?>
                                            <?php endwhile; ?>
                                        </nav>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    <?php $row++; ?>
                    <?php endwhile; ?>
                    <?php if($extra == 'onclick-1') : ?>
                        <div class="row text-right">
                            <div class="column">
                                <a href="#" class="topic-buttton moreTopics-1"><i class="fa fa-arrow-right" aria-hidden="true"></i> View More</a>
                            </div>
                        </div>
                    <?php endif; ?>
                <?php endif; ?>
            </section>

            <section id="learning" class="learning-container" data-magellan-target='learning'>
                <div class="row">
                    <div class="column">
                        <h3>Learning</h3>
                        <p><?php the_field('dd_learning_text'); ?></p>
                    </div>
                </div>
                <?php if(have_rows('dd_learning_sections')) : ?>
                <?php $row = 1; ?>
                    <?php while(have_rows('dd_learning_sections')) : the_row(); ?>
                        <?php $extra = ''; ?>
                        <?php if($row > 3) {
                            $extra = 'onclick-2';
                        } ?>
                        <div class='row topic-row <?php echo $extra; ?>'>
                            <div class='small-12 large-4 columns'>
                                <h5><?php the_sub_field('dd_learning_mod_title'); ?></h5>
                                <p><?php the_sub_field('dd_learning_mod_text');?></p>
                            </div>
                            <div class='small-12 large-8 columns show-for-large'>
                                <?php if(have_rows('dd_learning_modules')) : ?>
                                    <div class="orbit" role="region" aria-label="Resources" data-orbit> 
                                        <?php $slides = 0;?>
                                        <div class='resource-group  orbit-slide'>
                                            <?php $slides++; ?>
                                            <div class='row'>
                                                <?php $count = 0; ?>
                                                <?php while(have_rows('dd_learning_modules')) : ?>
                                                    <?php if ($count > 0 && ($count % 2 == 0)) :?>
                                                        </div>
                                                        </div>

                                                        <div class='resource-group  orbit-slide'>
                                                            <?php $slides++; ?>
                                                            <div class='row'>
                                                    <?php endif; ?>   
                                                    <?php the_row(); ?>

                                                    <div class='small-12 large-6 columns'>
                                                        <div class='mod-box'>
                                                            <div class='mod-image' style='background-image: url(<?php the_sub_field('dd_learning_mod_box_image'); ?>);'>
                                                                <div class="gradient">
                                                                    <a href='<?php the_sub_field('dd_learning_mod_box_url'); ?>' class='btn btnWhite'>View Details</a>
                                                                </div>
                                                            </div>
                                                            <div class='mod-info'>
                                                                <h6><?php the_sub_field('dd_learning_mod_box_title'); ?></h6>
                                                                <p><?php the_sub_field('dd_learning_mod_box_text'); ?></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php $count++;?>
                                                                
                                                <?php endwhile; ?>
                                            </div>
                                        </div>
                                        <?php if($slides > 1) :?>
                                            <nav class="orbit-bullets">
                                                <?php $slide_count = 0; ?>
                                                <?php while($slides > 0) : ?>
                                                    <button <?php if($slide_count == 0) { echo "class='is-active'"; } ?>  data-slide="<?php echo $slide_count; ?>"><span class="show-for-sr">First slide details.</span><span class="show-for-sr">Current Slide</span></button>
                                                    <?php $slides--; ?>
                                                    <?php $slide_count++; ?>
                                                <?php endwhile; ?>
                                            </nav>
                                        <?php endif; ?>
                                    </div>
                                <?php endif; ?>
                            </div>
                            
                            <div class='hide-for-large small-12 large-8 columns'>
                                <?php if(have_rows('dd_learning_modules')) : ?>
                                    <div class="orbit" role="region" aria-label="Resources" data-orbit>
                                        <div class='row'>
                                            <?php while(have_rows('dd_learning_modules')) : the_row(); ?>
                                                <div class='small-12 large-6 columns orbit-slide'>
                                                    <div class='mod-box'>
                                                        <div class='mod-image' style='background-image: url(<?php the_sub_field('dd_learning_mod_box_image'); ?>);'>
                                                            <div class="gradient">
                                                                <a href='<?php the_sub_field('dd_learning_mod_box_url'); ?>' class='btn btnWhite'>View Details</a>
                                                            </div>
                                                        </div>
                                                        <div class='mod-info'>
                                                            <h6><?php the_sub_field('dd_learning_mod_box_title'); ?></h6>
                                                            <p><?php the_sub_field('dd_learning_mod_box_text'); ?></p>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php endwhile; ?>
                                        </div>
                                        <nav class="orbit-bullets">
                                            <?php $mobile_slides = 0 ?>
                                            <?php while(have_rows('dd_learning_modules')) : the_row(); ?>
                                                <button <?php if($mobile_slides == 0) { echo "class='is-active'"; } ?>  data-slide="<?php echo $mobile_slides; ?>"><span class="show-for-sr">First slide details.</span><span class="show-for-sr">Current Slide</span></button>
                                                <?php $mobile_slides++; ?>
                                            <?php endwhile; ?>
                                        </nav>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>
                        <?php $row++; ?>
                    <?php endwhile; ?>
                    <?php if($extra == 'onclick-2') : ?>
                        <div class="row text-right">
                            <div class="column">
                                <a href="#" class="topic-buttton moreTopics-2"><i class="fa fa-arrow-right" aria-hidden="true"></i> View More</a>
                            </div>
                        </div>
                    <?php endif; ?>

                <?php endif; ?>
            </section>

            <section id="connecting" class="connecting-container" data-magellan-target='connecting'>
                <div class="row">
                    <div class="column">
                        <h3>Connecting</h3>
                        <p><?php the_field('dd_connect_text'); ?></p>
                    </div>
                </div>
                <?php if(have_rows('dd_connect_sections')) : ?>
                    <?php $row = 1; ?>
                    <?php while(have_rows('dd_connect_sections')) : the_row(); ?>
                        <?php $extra = ''; ?>
                        <?php if($row > 3) {
                            $extra = 'onclick-3';
                        } ?>
                        <div class='row topic-row <?php echo $extra; ?>'>
                            <div class='small-12 large-4 columns'>
                                <h5><?php the_sub_field('dd_connect_mod_title'); ?></h5>
                                <p><?php the_sub_field('dd_connect_mod_text');?></p>
                            </div>
                            <div class='small-12 large-8 columns show-for-large'>
                                <?php if(have_rows('dd_connect_modules')) : ?>
                                    <div class="orbit" role="region" aria-label="Resources" data-orbit> 
                                        <?php $slides = 0;?>
                                        <div class='resource-group  orbit-slide'>
                                            <?php $slides++; ?>
                                            <div class='row'>
                                                <?php $count = 0; ?>
                                                <?php while(have_rows('dd_connect_modules')) : ?>
                                                    <?php if ($count > 0 && ($count % 2 == 0)) :?>
                                                        </div>
                                                        </div>

                                                        <div class='resource-group  orbit-slide'>
                                                            <?php $slides++; ?>
                                                            <div class='row'>
                                                    <?php endif; ?>   
                                                    <?php the_row(); ?>

                                                    <div class='small-12 large-6 columns'>
                                                        <div class='mod-box'>
                                                            <div class='mod-image' style='background-image: url(<?php the_sub_field('dd_connect_mod_box_image'); ?>);'>
                                                                <div class="gradient">
                                                                    <a href='<?php the_sub_field('dd_connect_mod_box_url'); ?>' class='btn btnWhite'>View Details</a>
                                                                </div>
                                                            </div>
                                                            <div class='mod-info'>
                                                                <h6><?php the_sub_field('dd_connect_mod_box_title'); ?></h6>
                                                                <p><?php the_sub_field('dd_connect_mod_box_text'); ?></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php $count++;?>
                                                                
                                                <?php endwhile; ?>
                                            </div>
                                        </div>
                                        <?php if($slides > 1) :?>
                                            <nav class="orbit-bullets">
                                                <?php $slide_count = 0; ?>
                                                <?php while($slides > 0) : ?>
                                                    <button <?php if($slide_count == 0) { echo "class='is-active'"; } ?>  data-slide="<?php echo $slide_count; ?>"><span class="show-for-sr">First slide details.</span><span class="show-for-sr">Current Slide</span></button>
                                                    <?php $slides--; ?>
                                                    <?php $slide_count++; ?>
                                                <?php endwhile; ?>
                                            </nav>
                                        <?php endif; ?>
                                    </div>
                                <?php endif; ?>
                            </div>
                            
                            <div class='hide-for-large small-12 large-8 columns'>
                                <?php if(have_rows('dd_connect_modules')) : ?>
                                    <div class="orbit" role="region" aria-label="Resources" data-orbit>
                                        <div class='row'>
                                            <?php while(have_rows('dd_connect_modules')) : the_row(); ?>
                                                <div class='small-12 large-6 columns orbit-slide'>
                                                    <div class='mod-box'>
                                                        <div class='mod-image' style='background-image: url(<?php the_sub_field('dd_connect_mod_box_image'); ?>);'>
                                                            <div class="gradient">
                                                                <a href='<?php the_sub_field('dd_connect_mod_box_url'); ?>' class='btn btnWhite'>View Details</a>
                                                            </div>
                                                        </div>
                                                        <div class='mod-info'>
                                                            <h6><?php the_sub_field('dd_connect_mod_box_title'); ?></h6>
                                                            <p><?php the_sub_field('dd_connect_mod_box_text'); ?></p>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php endwhile; ?>
                                        </div>
                                        <nav class="orbit-bullets">
                                            <?php $mobile_slides = 0 ?>
                                            <?php while(have_rows('dd_connect_modules')) : the_row(); ?>
                                                <button <?php if($mobile_slides == 0) { echo "class='is-active'"; } ?>  data-slide="<?php echo $mobile_slides; ?>"><span class="show-for-sr">First slide details.</span><span class="show-for-sr">Current Slide</span></button>
                                                <?php $mobile_slides++; ?>
                                            <?php endwhile; ?>
                                        </nav>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>
                        <?php $row++; ?>
                    <?php endwhile; ?>
                    <?php if($extra == 'onclick-3') : ?>
                        <div class="row text-right">
                            <div class="column">
                                <a href="#" class="topic-buttton moreTopics-3"><i class="fa fa-arrow-right" aria-hidden="true"></i> View More</a>
                            </div>
                        </div>
                    <?php endif; ?>
                <?php endif; ?>
            </section>
        </div>

<?php get_footer(); ?>

<script>
    
    (function($){
        
        $(document).ready(function(){
            $('.onclick-1').toggle();
            $('.onclick-2').toggle();
            $('.onclick-3').toggle();
        });
        
        $('.moreTopics-1').on('click', function(e){
            e.preventDefault();
            $('.onclick-1').toggle(function(){
                if ($(this).is(':visible')) {
                    $('.moreTopics-1').html('<i class="fa fa-arrow-right" aria-hidden="true"></i> View Less');                
               } else {
                    $('.moreTopics-1').html('<i class="fa fa-arrow-right" aria-hidden="true"></i> View More');                
               } 
            });

        });
        
        $('.moreTopics-2').on('click', function(e){
            e.preventDefault();
            $('.onclick-2').toggle(function(){
                if ($(this).is(':visible')) {
                    $('.moreTopics-2').html('<i class="fa fa-arrow-right" aria-hidden="true"></i> View Less');                
               } else {
                    $('.moreTopics-2').html('<i class="fa fa-arrow-right" aria-hidden="true"></i> View More');                
               } 
            });
        });
        
        $('.moreTopics-3').on('click', function(e){
            e.preventDefault();
            $('.onclick-3').toggle(function(){
                if ($(this).is(':visible')) {
                    $('.moreTopics-3').html('<i class="fa fa-arrow-right" aria-hidden="true"></i> View Less');                
               } else {
                    $('.moreTopics-3').html('<i class="fa fa-arrow-right" aria-hidden="true"></i> View More');                
               } 
            });
        });
    })(jQuery);
    
</script>
