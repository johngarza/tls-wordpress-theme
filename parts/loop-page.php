<article id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="article" itemscope itemtype="http://schema.org/WebPage">

    <section class="entry-content" itemprop="articleBody">
        <div class="row">
            <div class="small-12 columns">
                <?php the_content(); ?>
                <?php wp_link_pages(); ?>
            </div>
        </div>
    </section> <!-- end article section -->

					
</article> <!-- end article -->