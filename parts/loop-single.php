<article id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
				
    <section class="entry-content" itemprop="articleBody">
        <?php
            $remove = get_field('default_featured_image_placement');
            
            if($remove) : ?>
            <?php else : ?>
		<?php the_post_thumbnail('full'); ?>
            <?php endif; ?>
            <?php the_content(); ?>
	</section> <!-- end article section -->
													
</article> <!-- end article -->