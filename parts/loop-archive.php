


        <?php 
        $highlight_type = get_field('home_highlight_select_posts', 'option'); 
        if($highlight_type == 'recent') : ?>

            
                <div class="column">
                    <a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><h4><?php the_title(); ?></h4></a>
                </div>
        
        <?php elseif($highlight_type == 'choice') : ?>

                    <div class="column ms-item">
                        <div class="post-block">
                            <?php if(get_the_post_thumbnail_url()) : ?>
                            <a href="<?php the_permalink(); ?>">
                                <div class="post-featured-image" style="background-image: url(<?php the_post_thumbnail_url('large'); ?>)" >
                                    <?php $format = get_post_format();
                                    if($format == 'video') :?>
                                        <i class="fa fa-play-circle-o" aria-hidden="true"></i>
                                    <?php endif; ?>
                                </div>
                            </a>
                            <?php endif;  ?>
                                <div class="post-info">
                                    
                                    <a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><h4><?php the_title(); ?></h4></a>
                                    <?php if(get_the_excerpt()) : ?>
                                        <?php $format = get_post_format();
                                        if($format == 'video') :?>
                                            <p><?php the_excerpt(); ?></p>
                                            <a href="<?php the_permalink(); ?>">Watch & learn</a>
                                        <?php else : ?>
                                            <p><?php the_excerpt(); ?></p>
                                            <a href="<?php the_permalink(); ?>">Read More</a>
                                        <?php endif; ?>
                                     
                                    <?php endif; ?>
                                </div>
                        </div>
                    </div>

        <?php endif; ?>
