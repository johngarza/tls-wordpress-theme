<!-- By default, this menu will use off-canvas for small
	 and a topbar for medium-up -->

<div class="top-bar" id="top-bar-menu">
    <div class="row header-row align-middle">
	<div class="small-8 medium-12 xlarge-4 columns text-center xlarge-text-left">
		<ul class="menu">
			<li class='logo'><?php the_custom_logo(); ?></li>
		</ul>
	</div>
	<div class="small-12 xlarge-8 columns show-for-medium text-center xlarge-text-right main-nav">
		<?php joints_top_nav(); ?>	
	</div>
	<div class="small-4 columns show-for-small-only text-right">
		<ul class="menu mobile-menu">
                    <li><button class="menu-icon" type="button" data-toggle="off-canvas"></button></li> 
                    <li><a data-toggle="off-canvas"><?php _e( 'Menu', 'jointswp' ); ?></a></li>
		</ul>
	</div>
    </div>
</div>