<?php 

if(get_field('header_image')) {
    $hero_background = get_field('header_image');
} else {
    $hero_background = get_field('banner_image', 'option');
}

if(get_field('fancy_title')) {
    $title = get_field('fancy_title');
} else {
    $title = get_the_title();
}

include_once(ABSPATH . WPINC . '/rss.php');
$feed = 'http://faculty.utsa.edu/events/category/utsa-teaching-and-learning-services/feed';
$rss = fetch_feed($feed);
$rss->enable_order_by_date(false);
get_header(); ?>

    <section class="inner-hero-container" style="background-image: url(<?php echo $hero_background; ?>);">
        <div class="gradient">
            <div class="hero-header row">
                <div class="small-12 columns text-center">
                    <h1><?php echo $title; ?></h1>
                </div>
            </div>
        </div>
    </section>
	
	<div id="content">  
	
		<div id="inner-content" class="row">
	
		    <main id="main" class="large-8 medium-8 columns" role="main">
				
                        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

                            <?php get_template_part( 'parts/loop', 'page' ); ?>

                        <?php endwhile; endif; ?>							

                    </main> <!-- end #main -->
                    
                    <?php $menu = get_field('default_sidebar_menu'); ?>
		    <div class="small-12 large-4 columns default-sidebar">
                        <?php if($menu) :?>
                        <?php wp_nav_menu( array( 'menu' => $menu,'menu_id' => 'default-sidebar-menu' ) ); ?>
                         <?php endif;?> 
                        <?php if(have_rows('default_sidebar_buttons')) : ?>
                            <?php while(have_rows('default_sidebar_buttons')) : the_row(); ?>
                                <a href="<?php the_sub_field('default_sidebar_button_url');  ?>" class="btn btnCTA text-center"><?php the_sub_field('default_sidebar_button_text'); ?></a>
                            <?php endwhile; ?>
                        <?php endif; ?>
                        

                        <?php if (!is_wp_error( $rss ) ) :
                        $maxitems = $rss->get_item_quantity(3);
                        $rss_items = $rss->get_items(0, $maxitems);
                        if ($rss_items): ?>
                        
                        <h3 class="text-center">Upcoming Workshops</h3>
                        <div class="upcoming-workshops ue-sidebar">
                            <?php foreach ( $rss_items as $item ) : ?>
                                <div class="event-info">
                                    <div class="row collapse">
                                        <div class="small-12 columns">
                                            <a href="<?php echo $item->get_permalink(); ?>"><h4><?php echo $item->get_title(); ?></h4></a>
                                            <p><?php echo $item->get_date(); ?></p>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; ?> 

                        </div>
                        <?php endif; ?>
                    <?php endif; ?>
                        <?php get_sidebar(); ?>
                        <?php if(have_rows('default_large_buttons', 'option')) : ?>
                            <?php while(have_rows('default_large_buttons', 'option')) : the_row(); ?>
                                <div class="page-buttons">
                                    <a href='<?php the_sub_field('default_button_link_url'); ?>'><div class="button-image" style="background-image: url(<?php the_sub_field('default_button_image'); ?>);" ></div></a>
                                    <div class='button-info'>
                                        <h6><?php the_sub_field('default_button_title'); ?></h6>
                                        <p><?php the_sub_field('default_button_text'); ?></p>
                                        <a href='<?php the_sub_field('default_button_link_url') ?>' class='link'><i class="fa fa-arrow-right" aria-hidden="true"></i> <?php the_sub_field('default_button_link_text'); ?></a>
                                    </div>
                                </div>
                            <?php endwhile; ?>
                        <?php endif; ?>
                    </div>
		    
		</div> <!-- end #inner-content -->

	</div> <!-- end #content -->

<?php get_footer(); ?>

<script>

    (function($){

        $('#default-sidebar-menu li.menu-item-has-children > a').append('<div class="default-sidebar-close">+</div>');

        $('div.default-sidebar-close').on('click', function(e){
            e.preventDefault();

            $(this).text(function(i, v){
                return v === '+' ? '-' : '+';
            });

    $(this).parent().siblings('ul.sub-menu').toggle();
        });

    })(jQuery);

</script>