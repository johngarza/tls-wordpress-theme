<?php

/***********************************************************************************************/
/* Move Yoast To The Bottom */
/***********************************************************************************************/
function yoasttobottom() {
    return 'low';
}
add_filter( 'wpseo_metabox_prio', 'yoasttobottom');

function wpdocs_custom_excerpt_length( $length ) {
    return 30;
}
add_filter( 'excerpt_length', 'wpdocs_custom_excerpt_length', 999 );

// Changing excerpt more
function new_excerpt_more($more) {
    global $post;
    remove_filter('excerpt_more', 'new_excerpt_more'); 
    return '';
}
add_filter('excerpt_more','new_excerpt_more',11);

function upcomingEvents() {
    $today = date('Ymd');
    $args = array (
        'post_type' => 'workshops',
        'numberposts' => 4,
        'meta_query' => array(
                 array(
                    'key'		=> 'event_date',
                    'compare'	=> '>=',
                    'value'		=> $today,
                )
        ),
        'orderby'   => 'event_date',
        'order' => 'ASC'
    );
    return get_posts( $args );
}

 // Dashboard Widget
function dashboard_widget_function( $post, $callback_args ) {
        echo "<div style='text-align:center;'><img src='" . get_template_directory_uri() ."/assets/images/webtegrity_logo.png' height=60></div><br>";
        
        echo "<h4>Available Buttons:</h4>";
        echo "Use one of the following class names to create a styled button.<br>";
        echo "<ul><li>btnPrimary</li><li>btnCTA</li><li>btnWhite</li><li>btnSolidWhite</li><li>btnOrangeLine</li></ul>";
        echo "Ex. " . esc_html('<a href="ButtonURL" class="btn btnPrimary" >Button Text</a>') . "<br><br>";
       

}

// Function used in the action hook
function add_dashboard_widgets() {
	wp_add_dashboard_widget('dashboard_widget', 'Built-in Styles', 'dashboard_widget_function');
        
        global $wp_meta_boxes;
        $normal_dashboard = $wp_meta_boxes['dashboard']['normal']['core']; 
        $example_widget_backup = array( 'example_dashboard_widget' => $normal_dashboard['example_dashboard_widget'] );
        unset( $normal_dashboard['example_dashboard_widget'] );
        $sorted_dashboard = array_merge( $example_widget_backup, $normal_dashboard );
        $wp_meta_boxes['dashboard']['normal']['core'] = $sorted_dashboard;
}

// Register the new dashboard widget with the 'wp_dashboard_setup' action
add_action('wp_dashboard_setup', 'add_dashboard_widgets' );


function my_modify_main_query( $query ) {
    if ( $query->is_home() && $query->is_main_query() ) { // Run only on the homepage
        $query->query_vars['cat'] = -22; // Exclude my featured category because I display that elsewhere
        $query->query_vars['posts_per_page'] = -1; // Show only 5 posts on the homepage only
    }
}
// Hook my above function to the pre_get_posts action
add_action( 'pre_get_posts', 'my_modify_main_query' );