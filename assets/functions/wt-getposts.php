<?php

/***********************************************************************************************/
/* Get Posts */
/***********************************************************************************************/

function newsPosts() {
	$args = array( 
            'posts_per_page' => 3,
            );
	return get_posts( $args );
}

function featuredPost() {
	$args = array( 
            'posts_per_page' => 1, 
            'category_name' => 'featured'
            
            );
	return get_posts( $args );
}