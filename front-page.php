<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
get_header(); ?>
<section class="hero-container" style="background-image: url(<?php the_field('home_hero_background_image', 'option');?>);">
    <div class="gradient">
        <div class="hero-header row">
            <div class="small-12 columns text-center">
                <h1><?php the_field('home_hero_header', 'option'); ?></h1>
                <p><?php the_field('home_hero_text', 'option'); ?></p>
                <a href='<?php the_field('home_hero_cta_button_url', 'option'); ?>' class='btn btnCTA'><?php the_field('home_hero_cta_button_text', 'option'); ?></a>
                <a href='<?php the_field('home_hero_primary_button_url', 'option'); ?>' class='btn btnWhite'><?php the_field('home_hero_primary_button_text', 'option'); ?></a>
            </div>
        </div>
    </div>
</section>
<section class="utsa-tabs">
    <?php if(have_rows('home_tabs_repeater', 'option')) : $i = 1; ?>
        <div class="expanded row tabs collapse" data-responsive-accordion-tabs="tabs small-accordion large-tabs" id="example-tabs">
            <?php while(have_rows('home_tabs_repeater', 'option')) : the_row(); ?>           
                <div class="small-12 large-3 columns tabs-title <?php if($i == 1) { echo ' is-active'; } ?>"><a href="#panel-<?= $i; ?>"><?php the_sub_field('home_tabs_title'); ?></a></div>
                <?php $i++; ?>
            <?php endwhile; ?>
        </div>
    <?php endif; ?>
    <?php if(have_rows('home_tabs_repeater', 'option')) : $i = 1; ?>
        <div class="tabs-content" data-tabs-content="example-tabs">
            <?php while(have_rows('home_tabs_repeater', 'option')) : the_row();  ?>
                <div class="tabs-panel <?php if($i == 1) { echo ' is-active'; } ?>" id="panel-<?= $i; ?>">
                    <div class="expanded row collapse">
                        <div class="small-12 large-6 columns tabs-image" style="background-image: url(<?php the_sub_field('home_tabs_content_background'); ?>);"></div>
                        <div class="small-12 large-6 columns tab-content-container">
                            <div class="tab-content">
                                <h2><?php the_sub_field('home_tabs_content_header'); ?></h2>
                                <?php the_sub_field('home_tabs_content'); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <?php $i++; ?>
            <?php endwhile; ?>
        </div>
    <?php endif; ?>
</section>
<section class="about-us">
    <div class="expanded row align-middle collapse">
        <div class="small-12 large-6 columns text-center large-text-right">
            <div class="about-content">
                <h2><?php the_field('home_about_header', 'option'); ?></h2>
                <?php the_field('home_about_content', 'option'); ?>
            </div>         
        </div>
        <div class="small-12 large-6 columns">
            <div class="about-us-image">
                <?php 

                    $image = get_field('home_about_image', 'option');

                    if( !empty($image) ): ?>

                            <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" title="<?php echo $image['title']; ?>" />

                <?php endif; ?>
            </div>
        </div>
    </div>
</section>
<section class="testimonials" style="background-image: url(<?php the_field('home_testimonial_background_image', 'option'); ?>); ">
    <div class="gradient">
        <div class="orbit" role="region" aria-label="Testimonial Slider" data-orbit >
            <div class="row align-middle">
                <div class="small-6 columns text-left">
                    <h2><?php the_field('home_testimonial_header', 'option'); ?></h2>
                </div>
                <div class="small-6 columns text-right">
                    <div class="orbit-controls">
                        <button class="orbit-previous"><span class="show-for-sr">Previous Slide</span><i class="fa fa-chevron-left" aria-hidden="true"></i></button>
                        <button class="orbit-next"><span class="show-for-sr">Next Slide</span><i class="fa fa-chevron-right" aria-hidden="true"></i></button>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="small-12 columns">
                    <?php if(have_rows('home_testimonials_repeater', 'option')) : $i = 1; ?>
                        <ul class="orbit-container">
                            <?php while(have_rows('home_testimonials_repeater', 'option')) : the_row(); ?>
                                 <li class="<?php if($i == 1){ echo 'is-active'; } ?> orbit-slide">
                                     <div class="testimonial-block">
                                         <blockquote>
                                             <p><?php the_sub_field('home_testimonial_quote'); ?></p>
                                             <span></span><h5><?php the_sub_field('home_testimonial_name'); ?></h5> - <h6><?php the_sub_field('home_testimonial_title'); ?></h6>
                                         </blockquote>
                                    </div>
                                </li>
                                <?php $i++; ?>
                            <?php endwhile; ?>
                        </ul>
                    <?php endif; ?>
                 </div>
            </div>
        </div>
    </div>
</section>
<section class="highlights">
    <div class="row text-center">
        <div class="column">
            <h2><?php the_field('home_highlights_header', 'option'); ?></h2>
        </div>
    </div>
    <div class="row small-up-1 large-up-3" id="ms-container">
        <?php 
        $highlight_type = get_field('home_highlight_select_posts', 'option'); 
        if($highlight_type == 'recent') :
            $posts = newsPosts();
            foreach($posts as $post) :?>
            
                <div class="column ms-item">
                        <div class="post-block">
                            <?php if(get_the_post_thumbnail_url()) : ?>
                            <a href="<?php the_permalink(); ?>">
                                <div class="post-featured-image" style="background-image: url(<?php the_post_thumbnail_url('large'); ?>)" >
                                    <?php $format = get_post_format();
                                    if($format == 'video') :?>
                                        <i class="fa fa-play-circle-o" aria-hidden="true"></i>
                                    <?php endif; ?>
                                </div>
                            </a>
                            <?php endif;  ?>
                                <div class="post-info">
                                    
                                    <a href="<?php the_permalink(); ?>"><h4><?php the_title(); ?></h4></a>
                                    <?php if(get_the_excerpt()) : ?>
                                        <?php $format = get_post_format();
                                        if($format == 'video') :?>
                                            <p><?php the_excerpt(); ?></p>
                                            <a href="<?php the_permalink(); ?>">Watch & learn</a>
                                        <?php else : ?>
                                            <p><?php the_excerpt(); ?></p>
                                            <a href="<?php the_permalink(); ?>">Read More</a>
                                        <?php endif; ?>
                                     
                                    <?php endif; ?>
                                </div>
                        </div>
                    </div>
        
            <?php endforeach;?>
        <?php elseif($highlight_type == 'choice') :
            $posts = get_field('home_highlighted_posts', 'option');
                if($posts) : 
                foreach($posts as $post) : setup_postdata($post);?>
            
                    <div class="column ms-item">
                        <div class="post-block">
                            <?php if(get_the_post_thumbnail_url()) : ?>
                            <a href="<?php the_permalink(); ?>">
                                <div class="post-featured-image" style="background-image: url(<?php the_post_thumbnail_url('large'); ?>)" >
                                    <?php $format = get_post_format();
                                    if($format == 'video') :?>
                                        <i class="fa fa-play-circle-o" aria-hidden="true"></i>
                                    <?php endif; ?>
                                </div>
                            </a>
                            <?php endif;  ?>
                                <div class="post-info">
                                    
                                    <a href="<?php the_permalink(); ?>"><h4><?php the_title(); ?></h4></a>
                                    <?php if(get_the_excerpt()) : ?>
                                        <?php $format = get_post_format();
                                        if($format == 'video') :?>
                                            <p><?php the_excerpt(); ?></p>
                                            <a href="<?php the_permalink(); ?>">Watch & learn</a>
                                        <?php else : ?>
                                            <p><?php the_excerpt(); ?></p>
                                            <a href="<?php the_permalink(); ?>">Read More</a>
                                        <?php endif; ?>
                                     
                                    <?php endif; ?>
                                </div>
                        </div>
                    </div>
                <?php endforeach; wp_reset_postdata(); ?>
            <?php endif; ?>
        <?php endif; ?>
    </div>
</section>

<?php get_footer(); ?>
