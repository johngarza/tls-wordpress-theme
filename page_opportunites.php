<?php

/*
 * Template Name: Opportunities
 */

if(get_field('header_image')) {
    $hero_background = get_field('header_image');
} else {
    $hero_background = get_field('banner_image', 'option');
}

if(get_field('fancy_title')) {
    $title = get_field('fancy_title');
} else {
    $title = get_the_title();
}


include_once(ABSPATH . WPINC . '/rss.php');
$feed = 'http://faculty.utsa.edu/events/category/utsa-teaching-and-learning-services/feed/?order=ASC';
$rss = fetch_feed($feed);
$rss->enable_order_by_date(false);


get_header(); ?>

<section class="inner-hero-container" style="background-image: url(<?php echo $hero_background; ?>);">
    <div class="gradient">
        <div class="hero-header row">
            <div class="small-12 columns text-center">
                <h1><?php echo $title; ?></h1>
                <p><?php the_field('op_hero_text'); ?></p>
            </div>
        </div>
    </div>
</section>

<section class="utsa-tabs dd-tabs">
   <div class="expanded row tabs collapse sticky" id="example-tabs" data-magellan>
       <div class="small-12 large-4 columns tabs-title"><a href="#workshops">Workshops<span><i class="fa fa-arrow-down" aria-hidden="true"></i>Learn More</span></a></div>
       <div class="small-12 large-4 columns tabs-title"><a href="#programs">Programs<span><i class="fa fa-arrow-down" aria-hidden="true"></i>Learn More</span></a></div>
       <div class="small-12 large-4 columns tabs-title"><a href="#funding">Funding<span><i class="fa fa-arrow-down" aria-hidden="true"></i>Learn More</span></a></div>
   </div>
</section>

<div class="op-content entry-content">
    <section id="workshops" class="workshops-container" style="background-image: url(<?php the_field('op_workshops_background_image'); ?>);" data-magellan-target='workshops'>
        <div class="gradient">
            <div class="row">
                <div class="column">
                    <h3>Workshops</h3>
                    <p><?php the_field('op_workshops_text'); ?></p>
                </div>
            </div>
            <div class="row workshop-content">
                <div class="small-12 large-6 columns">
                    <?php if(have_rows('op_workshops_button_repeater')) : ?>
                        <?php while(have_rows('op_workshops_button_repeater')) : the_row(); ?>
                            <a href="<?php the_sub_field('op_workshops_button_url'); ?>" class="btn <?php the_sub_field('op_workshops_button_color'); ?> btnHeader"><?php the_sub_field('op_workshops_button_text'); ?></a>
                        <?php endwhile; ?>
                    <?php endif; ?>
                </div>
                <div class="small-12 large-6 columns">
                    <?php if (!is_wp_error( $rss ) ) :
                        $maxitems = $rss->get_item_quantity(3);
                        $rss_items = $rss->get_items(0, $maxitems);
                        if ($rss_items): ?>

                        <h3>Upcoming Workshops</h3>
                        <div class="upcoming-workshops">
                            <?php foreach ( $rss_items as $item ) : ?>
                                <div class="event-info">
                                    <div class="row collapse">
                                        <div class="small-12 columns">
                                            <a href="<?php echo $item->get_permalink(); ?>"><h4><?php echo $item->get_title(); ?></h4></a>
                                            <p><?php echo $item->get_date(); ?></p>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                            <div class="event-info">
                                <div class="row collapse">
                                    <div class="small-12 columns" style="text-align:center">
                                        <a href="https://faculty.utsa.edu/events/category/utsa-teaching-and-learning-services/"><h4>View all events <i class="fa fa-arrow-right" aria-hidden="true"></i></h4></a>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <?php endif; ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </section>

    <section id="programs" class="programs-container" data-magellan-target='programs'>
        <div class="row">
            <div class="column">
                <h3>Programs</h3>
                <p><?php the_field('op_programs_text'); ?></p>
            </div>
        </div>
        <div class="row text-center programs-container-row">
           <?php $post_objects = get_field('op_programs_programs');

            if( $post_objects ): ?>
                <?php foreach( $post_objects as $post): // variable must be called $post (IMPORTANT) ?>
                    <?php setup_postdata($post); ?>
                        <div class="small-12 medium-6 large-4 columns">
                            <a href="<?php the_permalink(); ?>">
                                <div class="more-boxes">
                                    <h4><?php the_title(); ?></h4>
                                    <?php the_excerpt(); ?>
                                    <p class="link"><i class="fa fa-arrow-right" aria-hidden="true"></i> Learn more</p>
                                </div>
                            </a>
                        </div>
                <?php endforeach; ?>
                <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
            <?php endif; ?>
        </div>
    </section>

    <section id="funding" class="funding-container" style="background-image: url(<?php the_field('op_funding_background_image'); ?>);" data-magellan-target='funding'>
        <div class="gradient">
            <div class="row">
                <div class="column">
                    <h3>Funding</h3>
                    <p><?php the_field('op_funding_text'); ?></p>
                </div>
            </div>
            <?php if(have_rows('op_funding_repeater')) : ?>
                <div class="row text-center more-resources-row align-center">
                    <?php while(have_rows('op_funding_repeater')) : the_row(); ?>
                        <div class="small-12 medium-4 columns">

                            <div class="more-boxes">
                                <a href="<?php the_sub_field('op_funding_page_url'); ?>"><h4><?php the_sub_field('op_funding_title'); ?></h4></a>
                                <?php the_sub_field('op_funding_description'); ?>
                                <div class="link-box">
                                    <?php if(get_sub_field('op_funding_details_link')) : ?>
                                        <a href="<?php the_sub_field('op_funding_details_link'); ?>" class="link details-link"><i class="fa fa-arrow-right" aria-hidden="true"></i> Application Details</a>
                                    <?php endif; ?>
                                    <?php if(get_sub_field('op_funding_apply_link')) : ?>
                                        <a href="<?php the_sub_field('op_funding_apply_link'); ?>" class="link apply-link"><i class="fa fa-arrow-right" aria-hidden="true"></i> Apply Now</a>
                                    <?php endif; ?>
                                </div>
                            </div>

                        </div>
                    <?php endwhile; ?>
                </div>
            <?php endif; ?>
        </div>
    </section>


</div>

<?php get_footer(); ?>
