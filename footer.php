				<footer class="footer" role="contentinfo">
					<div id="inner-footer" class="row text-center large-text-left">
                                                <div class="small-12 medium-6 large-4 columns">
                                                    <?php if (!function_exists('register_sidebar') || !dynamic_sidebar('footer-widget-1')):
                                                     endif; ?>
                                                </div>
                                                <div class="small-12 medium-6 large-4 columns contact-column">
                                                    <?php if (!function_exists('register_sidebar') || !dynamic_sidebar('footer-widget-2')):
                                                     endif; ?>
                                                </div>
                                                <div class="small-12 medium-6 large-2 columns">
                                                    <?php if (!function_exists('register_sidebar') || !dynamic_sidebar('footer-widget-3')):
                                                     endif; ?>
                                                </div>
                                                <div class="small-12 medium-6 large-2 columns">
                                                    <?php if (!function_exists('register_sidebar') || !dynamic_sidebar('footer-widget-4')):
                                                     endif; ?>
                                                </div>
                                            
                                        </div> <!-- end #inner-footer -->
                                        <div class="copyright-container">
                                            <div class="row">
                                                <div class="small-12 large-8 columns text-center medium-text-left">
                                                    <p class="source-org copyright">&copy; <?php echo date('Y'); ?> The University of Texas at San Antonio | One UTSA Circle, San Antonio, TX 78249 | Information: <a href="tel:2104584011">(210) 458-4011</a></p>
                                                    <nav role="navigation">
                                                        <?php joints_footer_links(); ?>
                                                    </nav>
                                                    </div>
                                                <div class="small-12 large-4 columns text-center medium-text-right">
                                                    <?php if(have_rows('social_icons', 'option')) :?>
                                                        <div class="social-container">
                                                            <?php while(have_rows('social_icons', 'option')) : the_row();?>
                                                            
                                                                <a href="<?php the_sub_field('social_icon_url'); ?>"><i class="fa <?php the_sub_field('social_icon'); ?>" aria-hidden="true"></i></a>

                                                            <?php endwhile; ?>
                                                        </div>
                                                    <?php endif; ?>
                                                </div>
                                            </div>
                                            <div>
                                                <div class="toTop" data-magellan>
                                                    <a href="#top" title="Back to Top"><i class="fa fa-arrow-up" aria-hidden="true"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        
				</footer> <!-- end .footer -->
			</div>  <!-- end .main-content -->
		</div> <!-- end .off-canvas-wrapper -->
		<?php wp_footer(); ?>
	</body>
</html> <!-- end page -->